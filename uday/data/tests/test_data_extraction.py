import hashlib
from uday.data import PacketLoader


class TestDataLoader():
    def test_dns_pkt_extraction(self):
        pl = PacketLoader('dns.pcap')
        pl.parse_pkts()
        pl.to_json()

        with open('dns-verified-log.json') as f:
            data = f.read().encode('utf-8')
            md5_verified = hashlib.md5(data).hexdigest()

        with open('dns.json') as f:
            data = f.read().encode('utf-8')
            md5_to_verify = hashlib.md5(data).hexdigest()

        assert (md5_verified == md5_to_verify)
