"""
Extracting IP packets
"""

import dpkt
from dpkt.utils import inet_to_str
from typing import Dict

from uday.data import layer4

IP = dpkt.ip.IP
IP6 = dpkt.ip6.IP6


def _parse_ip_packet(ip: IP) -> Dict:
    """Parses IP Packet

    Parameters
    ----------
    pkt: IP
        IP packet

    data: Dict
        IP level data

    Returns
    -------
    pkts: Dict
        Parsed TCP/UDP information
    """
    data = {}
    data['ip_src'] = inet_to_str(ip.src)
    data['ip_dst'] = inet_to_str(ip.dst)

    if isinstance(ip, IP):
        data['proto'] = 'ipv4'
        data['ip_len'] = ip.len
        data['ttl'] = ip.ttl
    elif isinstance(ip, IP6):
        data['proto'] = 'ipv6'

    if isinstance(ip.data, dpkt.udp.UDP):
        data['l4'] = layer4._parse_udp_packet(ip.data)
    elif isinstance(ip.data, dpkt.tcp.TCP):
        data['l4'] = layer4._parse_tcp_packet(ip.data)

    return data
