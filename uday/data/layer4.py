"""
Methods to process and extract information from layer 4 protocols
- TCP and UDP
"""

import dpkt
from typing import Dict, List, Tuple
from dpkt.utils import inet_to_str

TCP = dpkt.tcp.TCP
UDP = dpkt.udp.UDP


def _parse_udp_packet(udp: UDP) -> Dict:
    """Parses UDP packet

    Parameters
    ----------
    udp: UDP
        UDP Packet

    data: Dict
        UDP Fields
    """
    data: Dict = {}
    data['proto'] = 'udp'
    data['sport'] = udp.sport
    data['dport'] = udp.dport
    data['ulen'] = udp.ulen
    if data['sport'] == 53 or data['dport'] == 53:
        data['l7'] = _parse_dns_packet(udp)
    return data


def _parse_dns_packet(l4) -> Dict:
    data: Dict = {}
    data['proto'] = 'dns'
    data['is_valid'] = 0
    try:
        dns = dpkt.dns.DNS()
        dns.unpack(l4.data)
        data['is_valid'] = 1
        data['dns_queries'] = []
        for query in dns.qd:
            data['dns_queries'].append((query.name, query.type))

        data['dns_answers'] = []
        for answer in dns.an:
            if answer.type == 5:
                data['dns_answers'].append((answer.name, answer.cname))
            elif answer.type == 1:
                data['dns_answers'].append(
                    (answer.name, inet_to_str(answer.ip)))
            else:
                continue
    except (dpkt.dpkt.NeedData, dpkt.dpkt.UnpackError, Exception):
        pass
    return data


def _parse_tcp_packet(tcp: TCP) -> Dict:
    """Parses TCP packet

    Parameters
    ----------
    tcp: TCP
        TCP Packet

    data: Dict
        TCP Fields
    """
    data = {}
    return data
