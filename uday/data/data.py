"""
Contains wrapper classes for reading pcap files
"""
import os
import csv
import json
import dpkt

from typing import List, Tuple


class DataLoader(object):
    """
    Abstract base class for other dataset objects.

    `DataLoader` objects are used to store representation
    of different types of data handled. Note that a `DataLoader`
    object cannot be instantiated directly. Instead you will
    need to instantiate a subclass of it.
    """
    def __init__(self) -> None:
        raise NotImplementedError()

    def __len__(self) -> int:
        """Get number of elements in the dataset

        Returns
        -------
        int
            The number of elements in the dataset
        """
        raise NotImplementedError()

    def to_json(self) -> None:
        """Dump dataset to a json file

        Parameters
        ----------
        fname: str
            The name of the json file.
        """
        raise NotImplementedError()


class PacketLoader(DataLoader):
    """
    The PacketLoader class loads raw packets and parse TCP/IP
       level information.

    Example
    -------
    >>> from uday.data import PacketLoader
    >>> pl = PacketLoader('/path/to/file.pcap')
    >>> pl.parse_pkts()
    >>> # to dump details to a CSV file
    ... pl.to_csv()
    >>> # to dump details to a JSON file
    ... pl.to_json()

    """
    def __init__(self, file_path: str) -> None:
        """
        Parameters
        ----------
        file_path: str
            Path to pcap file
        """
        self.file_path = file_path
        self.raw_pkts = self._read_pcap()

    def _read_pcap(self) -> List[Tuple]:
        fp = open(self.file_path, 'rb')
        try:
            capture = dpkt.pcap.Reader(fp)
        except ValueError as e_pcap:
            try:
                fp.seek(0, os.SEEK_SET)
                capture = dpkt.pcapng.Reader(fp)
            except ValueError as e_pcapng:
                raise Exception(
                    "File doesn't appear to be a PCAP or PCAPng: %s, %s" %
                    (e_pcap, e_pcapng))
        capture = list(capture)
        return capture

    def parse_pkts(self):
        capture = self.raw_pkts

        from uday.data import layer3
        parsed_pkts = []
        for timestamp, buf in capture:
            eth = dpkt.ethernet.Ethernet(buf)
            data = {}
            data['timestamp'] = timestamp

            # Check for all layer 3 protocols here
            if isinstance(eth.data, dpkt.ip.IP) or isinstance(
                    eth.data, dpkt.ip6.IP6):
                ip = eth.data
                data['l3'] = layer3._parse_ip_packet(ip)
            elif isinstance(eth.data, dpkt.icmp.ICMP):
                continue
            elif isinstance(eth.data, dpkt.arp.ARP):
                continue
            elif isinstance(eth.data, dpkt.rip.RIP):
                continue
            parsed_pkts.append(data)
        self.pkts = parsed_pkts
        return

    def to_json(self) -> None:
        """Dump dataset to a json file

        Examples
        --------
        >>> from uday.data import PacketLoader
        >>> pl = PacketLoader()
        >>> pl.to_json()

        Parameters
        ----------
        fname: str
            The name of the json file.
        """
        with open('pkts.json', 'w') as f:
            for pkt in self.pkts:
                json.dump(pkt, f)
                f.write('\n')

        with open('dns.json', 'w') as f:
            for pkt in self.pkts:
                if 'l3' in pkt.keys() and 'l4' in pkt['l3'].keys():
                    if 'l7' in pkt['l3']['l4'].keys():
                        l7 = pkt['l3']['l4']['l7']
                        if l7['proto'] == 'dns' and l7['is_valid'] == 1:
                            for i in range(0, len(l7['dns_queries'])):
                                dns_data = {}
                                dns_data['timestamp'] = pkt['timestamp']
                                dns_data['query_name'] = l7['dns_queries'][i][
                                    0]
                                dns_data['query_type'] = l7['dns_queries'][i][
                                    1]
                                json.dump(dns_data, f)
                                f.write('\n')
                            for i in range(0, len(l7['dns_answers'])):
                                dns_data = {}
                                dns_data['timestamp'] = pkt['timestamp']
                                dns_data['query'] = l7['dns_answers'][i][0]
                                dns_data['answer'] = l7['dns_answers'][i][1]
                                json.dump(dns_data, f)
                                f.write('\n')

        return

    def to_csv(self) -> None:
        """Dumps data to a CSV file

        Examples
        --------
        >>> from uday.data import PacketLoader
        >>> pl = PacketLoader('/path/to/file.pcap')
        >>> pl.to_csv()

        Parameters
        ----------
        fname: str
            The name of the CSV file
        """
        # all possible fields
        fieldnames = ['timestamp', 'ip_src', 'ip_dst', 'ip_len', 'ttl']
        with open('pkts.json', 'w', newline='') as f:
            writer = csv.DictWriter(f, fieldnames=fieldnames)
            writer.writeheader()
            for pkt in self.pkts:
                writer.writerow(pkt)
        return


class FlowLoader(PacketLoader):
    """Generates flow features from packets

    """
    def __init__(self):
        self.flows = None
        return
