"""
Test class for lexical featurizer
"""
from uday.feat import LexicalFeaturizer

class TestLexicalFeaturizer():

    def test_length_related_features(self):
        lf = LexicalFeaturizer()
        query = "www.google.com"
        len_features = lf._get_length_related_features(query)
        assert(len_features['query_len'] == 14)
        assert(len_features['avg_token_len'] == 4.0)

    def test_count_related_features(self):
        lf = LexicalFeaturizer()
        query = "www.google.com"
        count_features = lf._get_count_related_features(query)
        assert(count_features['alpha_count'] == 12)
        assert(count_features['symbol_count'] == 2)
        assert(count_features['digit_count'] == 0)
        assert(count_features['count%'] == 0)
        assert(count_features['count?'] == 0)
        assert(count_features['count-'] == 0)
        assert(count_features['count='] == 0)
        assert(count_features['count.'] == 2)
        assert(round(count_features['digit_ratio'], 3) == 0.022)
        assert(round(count_features['alpha_ratio'], 3) == 0.822)
        assert(round(count_features['symbol_ratio'], 3) == 0.156)
        assert(round(count_features['alpha_to_digit'], 3) == 37.0)
        assert(round(count_features['alpha_to_symbol'], 3) == 5.286)

    def test_entropy_related_features(self):
        lf = LexicalFeaturizer()
        query = "www.google.com"
        entropy_features = lf._get_entropy_related_features(query)
        assert(round(entropy_features['entropy'], 3) == -1.97)

    def test_character_continuity_rate(self):
        lf = LexicalFeaturizer()
        ccr = lf._get_character_continuity_rate('abc567-gt')
        assert(round(ccr['ccr'], 3) == 0.778)
