"""
Base class for featurization
"""
import numpy as np
from typing import Iterable, Any

class Featurizer(object):
    """Abstract class for calculating a set of features

    This class is abstract and cannot be invoked directly. To
    make your own featurizer, make a child class which implements
    `_featurize` method for calculating features for single data point.
    """

    def featurize(self, datapoints):
        """Calculate features for datapoints

        Parameters
        ----------
        datapoints: Iterable[Any]
            A sequence of objects that you'd like to featurize

        Returns
        -------
        featurized_datapoints: Iterable[Any] 
            A numpy array or a sequential object containing the featurized
            datapoints.
        """
        features = []
        for sample in datapoints:
            features.append(self._featurize(sample))
        return features

    def _featurize(self, sample):
        """Calculate features for a single datapoint

        Parameters
        ----------
        sample: Any
            A single data point
        Returns
        -------
        featurized_sample: Any
            A featurized data point
        """
        raise NotImplementedError()
