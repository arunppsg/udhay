# URL analysis

Malwares can be detected by their domains/URL's to which they
connect to. Performing lexical analysis on the URL can help in
detection of phishing, spamming URLs.

Another common phishing techniques is detection of mis-leading URLs
like - paypa1, ad0be, faceb00k.com. We need to develop techniques to
detect such URLs. Genetic algorithms can be put to use.
