Coding Conventions
=================

The project uses the following coding conventions to keep
the  code healthy.

Code Formatting
---------------

.. _`YAPF`: https://github.com/google/yapf

Whenever you modify a file, run :code:`yapf` on it:

.. code-block:: bash

  yapf -i <modified file>

`YAPF`_ is a formatter for python files. Whenever modifications
are made on a file, YAPF helps in ensuring that the style is consistent
with the rest of codebase. The YAPF version used is 0.31.0.

Linting
-------

.. _`Flake8`: https://github.com/pycqa/flake8

Flake8 is used to check code syntax. Whenver you modify a
file, run :code:`flake8` on it:

.. code-block:: bash

    flake8 <modified-file> --count

If it returns 0, then it means that your code passes :code:`flake8` check.

Type Annotations
----------------

.. _`Mypy`: https://github.com/python/mypy

Type annotations are very important for keeping the code healthy.
The project uses :code:`mypy`. `Mypy`_ is an optional static type checker
for Python. Use mypy on your modified files as:

.. code-block:: bash
    
    mypy <modified-file>

If there are no errors, it returns :code:`Success` otherwise the
:code:`error` message is returned.

Docstrings
----------

.. _`doctests`: https://docs.python.org/3/library/doctest.html

To test docstrings, run

.. code-block:: bash

    python -m doctest <modifies-file>

For more information, see `doctests`_  manual.

Unit tests
----------

To perform unit tests locally, run

.. code-block:: bash

    python -m pytest <modified-file>
