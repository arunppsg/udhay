####################
Detection Techniques
####################

This section highlights techniques which are to be integrated 
with ``uday`` and it also presents a summary of past approaches
considered.

The goal of detection is to find new suspicious connections
in the network traffic. There are many points of detection of a
malware according to which techniques can be developed. They are

.. contents:: Content
    :local:
    

DNS Activity
------------
The following approaches can be used for detecting malicious
domain names:

* Lexical Analysis of Domain Names
  This approach uses a classifier (decision tree) built using features from domain names.

* Co-occurrence rank
    The approach classifies malicious domains based on the malicious score of co-occurring
    domains. Algorithms which can be used are:

    * K-nearest neighbors - an average of neighbors scores
    * A page rank based approach which makes a graph of co-occurring domains. Scores to
      new domains could be assigned as a weighted average of the neighbor's score.
    * Document similarity based approach: domains occurring in a window form a document
      and it’s tf-idf representation is learnt. The tf-idf feature vector is used to build a classifier.

* Spike rank
  
  This technique works by detecting time-series patterns on DNS request volume to
  domains. The pattern helps in detecting anomalies and attacks such as sudden spikes
  in DNS request volumes. We implemented an anomaly detection based approach. Domains
  with similar traffic patterns are grouped together and anomaly detection is performed
  using the group features.


Network Activity
----------------

Network traffic flow features like number of in/out bytes bytes, in/out packets,
protocol, source port and destination port, uniqueness of a flow (inverse of the
number of times the flow is seen), average size of packets etc are collected.
A classifier can be built to distinguish benign and malicious flows.

Past Approaches:

* TadGAN - In this approach, we made predictions of malicious connections
  from a time-series data of IP addresses. We were not able to test it due
  to lack of a labelled dataset.


OS Activity
-----------

Fingerprinting techniques
-------------------------

The fingerprinting techniques which can be used are:

* File hash analysis
  This approach is prone to false negatives because many malwares
  can escape detection easily.

* Fingerprinting of host by their TLS fingerprints


Observations
^^^^^^^^^^^^

* For the same payload, hashes of encrypted payload observed from
  different host machines were not the same because different encryption
  keys were used in different hosts.
* Different encryption of malware results in different payloads.
  Enumerating hash values across all different combinations is not feasible.

Signature matching
------------------

* SNORT-3 is intrusion prevention system based on signature matching.
* YARA rules can be used to perform signature matching (see
  `awesome-yara <https://github.com/InQuest/awesome-yara>`_)

Analysis of Binaries
--------------------

* Binaries or files can be executed in a sandbox (ex: cuckoo sandbox) and their properties
  can be checked.
* The process call graph binaries of a binary or a code sample can be analyzed
  to detect malware (more clarity is required here).

Analysis of Files
-----------------

* Files like PDF's need to be analyzed for presence of malware (ref `paper <https://personal.utdallas.edu/~muratk/courses/dmsec_files/Malicious_PDF_Detection_ACSAC_12.pdf>`_)


SSL Decryption
--------------

* SSL decryption gives insights by analyzing raw unencrypted traffic but it comes at the
  cost of privacy.

Selective Proxy
---------------

* For susceptible applications, a selective proxy can be used which can give special attention to those connections.

Geographical analysis of requests
---------------------------------

In this analysis, we perform analysis by looking at the geographic
distribution of IP address.

Knowledge Graph
---------------

A key component of knowledge graph is t build the topology of the network
It maps who is taking to whom.

Abstract Syntax Trees
---------------------

Others
------

* WHOIS database

Challenges
----------
Some of the challenges are highlighted here:

* There is a vast amount of data which is to be analysed for potential
  malicious content
* When a host uses DNS over HTTPS, the detection techniques cannot be used.
