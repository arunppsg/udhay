Design Choices
==============

This document outlines the choice of language used and
other aspects of the tool from engineering perspectives. It also
includes the developmental plan and scopes of Uday.

Choice of programming language
------------------------------
Python can be used for building prototypes since prototypes can be 
built and tested quickly. 

Other choices:

* Go helps in building a simple, reliable and efficient software. Using Go
  will increase productivity in making the end application.
* C++ - SNORT-3 is written in C++. Key advantages is that it is low level, can support
  a wide variety of operations and highly efficient.

Choice of database
------------------
Uday requires a database for storing information. Choices under consideration are:

* Postgresql
* MongoDB

Developmental Vision
--------------------

Monolithic
^^^^^^^^^^
Uday is designed to be a one stop solution for enterprise protection. Hence,
all code and features will be in a single codebase.

Orthogonality
^^^^^^^^^^^^^
Two lines are independent if they are orthogonal. Components/modules of Uday
should be orthogonal. This helps in producing systems which are easy to
design, build, test and extend.

Testing
^^^^^^^
Testing will be an integral part of Uday. Each individual component
should have its own unit tests. On testing of machine learning models,
testing and benchmarking should be performed on a collected dataset
so that performances of different approaches can be measure. This invovles
maintaining a dataset for testing methods and techniques.

Cluster
^^^^^^^
Uday is meant to be distributed system. To handle high workloads, Uday
should spread the workload across many cores or physical machines. A
cluster architecture will allow Uday to monitor at very high speed
than a multi-threaded process.

Scaling Uday
------------

The following features will be of great value to Uday:

* Performing analysis on live traffic
* Analytics via dashboard
