.. _howto-docs:

#############################
Contributing to Documentation
#############################

The project uses `Sphinx <http://www.sphinx-doc.org/>`_ in
combination with NumPy style documentation conventions.

Building Documentation
======================

From the root directory of the project, write::

    cd doc

Now, install all the necessary dependencies with::

    pip install -r doc_requirements.txt

For building html documentation, run::
    
    make html

The following produces PDF documentation, run::

    make latexpdf

Note that the above requires a working LaTeX installation. 
