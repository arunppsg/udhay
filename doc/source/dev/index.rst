
Design
======

.. toctree::
    :glob:
    :maxdepth: 1
    :caption: Design

    introduction
    coding-conv
    docs-contrib
    design-choices
    architecture
    detect-techniques 
    resources
