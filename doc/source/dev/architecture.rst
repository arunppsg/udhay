Architecture
============

Malware detection has lot of different components like 
OS level anaysis, file analysis, network traffic analysis and each
component will require it's own data and algorithm. Each component
of the model should be independent of the other components and have
its own tests and documentation.

The :code:`data` module is used for collecting or reading data and 
processing it.

The analysis modules are organized by the type of data which is
used in the analysis. 

The :code:`feat` module is used to featurize the data for different 
analysis. The :code:`core` modules acts as an interface between the
:code:`data` module and the :code:`analysis` modules. It gets 
data from the data module and uses the algotihms of the analysis 
modules to perform the analysis inference.

Finally, the :code:`core` module builds a knowledge graph
of the system and uses it for inference.
