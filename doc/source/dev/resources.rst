Resources
---------

This section contains relevant publications and other references related
to this project.

* `https://github.com/rshipp/awesome-malware-analysis <https://github.com/rshipp/awesome-malware-analysis>`_


This document suggests the possible datasets which
can be used for the analysis purposes. The dataset should
contain network data as well as OS-level data.

Raw network packets:

* `UNSW-NB15 <https://research.unsw.edu.au/projects/unsw-nb15-dataset>`_

URL dataset:

* `ICSX URL 2016 dataset <https://www.unb.ca/cic/datasets/url-2016.html>`_

References:

* `Vizsec <https://vizsec.org/data/>`_
* `Caida <https://www.caida.org/catalog/datasets/overview/>`_
* `Canadian Institute for Cybersecurity datasets <https://www.unb.ca/cic/datasets/index.html>`_
