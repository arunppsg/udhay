
Uday User Guide
===============

.. toctree::
    :glob:
    :maxdepth: 1
    :caption: User Guide 

    whatisuday 
    installation  
    quickstart
