.. _whatisuday:

*************
What is Uday?
*************

Uday is a python package for detecting malwares in a network.
At the core of Uday are the `data` module and `analysis` modules
which helps in processing input data and performing analysis
respectively.
