.. uday documentation master file, created by
   sphinx-quickstart on Tue Sep 21 12:25:09 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

##################
Documentation
##################

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   User Guide <user/index>
   API Reference <api_reference/index>
   Dashboard <dashboard>
   Development <dev/index>
