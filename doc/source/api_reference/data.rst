Data
====

The :code:`uday.data` provides API for handling data. Uday handles
network data in various levels like packet level, protocol level,
flow level etc. as well as it has to handle other data like files
for op-code analysis, URLs for DNS analysis, OS Activity data etc.

.. contents:: Contents
    :local:

Data Loaders
------------

PacketLoader
^^^^^^^^^^^^

.. autoclass:: uday.data.data.PacketLoader
    :members: 

FlowLoader
^^^^^^^^^^

.. autoclass:: uday.data.data.FlowLoader
    :members:

Methods
-------

Base Classes (for development)
------------------------------

DataLoader
^^^^^^^^^^

The :code:`uday.data.DataReader` is a abstract class for all data readers.
This class should never be directly initialized.

.. autoclass:: uday.data.data.DataLoader
    :members:
