Featurizer
==========

The :code:`uday.feat` provides API for featurizing/preparing data to
various formats for analysis. For example, DNS analysis requires features of
domain name while flow analysis requires flow-based features. The featurizer
modules helps in preparing the data for different analysis purposes.

.. contents:: Contents
    :local:

DNS Featurizer
--------------

LexicalFeaturizer
^^^^^^^^^^^^^^^^^

.. autoclass:: uday.feat.lexical_featurizer.LexicalFeaturizer
    :members: 

Methods
-------

Base Classes (for development)
------------------------------

Featurizer
^^^^^^^^^^

The :code:`uday.feat.base_classes.Featurizer` is a abstract class for all featurizers.
This class should never be directly initialized.

.. autoclass:: uday.feat.base_classes.Featurizer
    :members:
