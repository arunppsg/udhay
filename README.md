# Uday

Uday - A malware detection framework in a network.
Given a network traffic sample, Uday checks for the presence
of malware in that traffic sample. It can also be used for
detecting malware in real time.
